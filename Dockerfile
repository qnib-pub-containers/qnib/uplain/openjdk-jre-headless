# syntax = docker/dockerfile:1.4

ARG FROM_IMG_REGISTRY=registry.gitlab.com
ARG FROM_IMG_REPO=qnib-pub-containers/qnib/uplain
ARG FROM_IMG_NAME=init
ARG FROM_IMG_TAG="2023-05-31"

FROM ${FROM_IMG_REGISTRY}/${FROM_IMG_REPO}/${FROM_IMG_NAME}:${FROM_IMG_TAG}


RUN <<eot bash
 apt update
 apt install -y openjdk-17-jre-headless
 rm -rf /var/lib/apt/lists/*
eot